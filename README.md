# xyz2mbtiles

#### 介绍
将xyz格式的栅格瓦片转换为单个.mbtiles文件，便于数据迁移。

#### 软件架构
该项目依赖sqlite（ https://github.com/sqlite/sqlite.git ）、CppSQLite3U（ http://softvoile.com/development/CppSQLite3U/ ，有些适应性修改）等，目前仅能将.png图片存入.mbtiles文件。
             
#### 安装教程
##### 安装教程（Windows）
1.  git clone https://gitee.com/zjp369/xyz2mbtiles.git
2.  cd xyz2mbtiles
3.  mkdir build
4.  cd build
5.  call D:\Green\VS2015\VC\bin\x86_amd64\vcvarsx86_amd64.bat 
6.  cmake -G "Visual Studio 14 2015 Win64" .. 
7.  cmake --build . --target xyz2mbtiles --config release
8.  devenv.com xyz2mbtiles.sln /build "Release|x64" /project xyz2mbtiles.vcxproj
9.  7/8中执行任何一个步骤均可生成可执行文件

##### 安装教程（Linux）
1.  git clone https://gitee.com/zjp369/xyz2mbtiles.git
2.  cd xyz2mbtiles
3.  mkdir build
4.  cd build
5.  apt-get install -y build-essential cmake
6.  cmake .. 
7.  make

#### 使用说明

##### 使用说明（Windows）
1.  复制xyz2mbtiles.exe到png格式的xyz文件所在目录（z级目录）。
2.  双击xyz2mbtiles.exe启动程序，耐心等待程序运行完成。
3.  程序运行过程中会输出一些提示信息，最后会显示总共输入多少个png文件。按下任意键退出运行窗口。

##### 使用说明（Linux）
1.  复制xyz2mbtiles到png格式的xyz文件所在目录（z级目录）。
2.  ./xyz2mbtiles启动程序，耐心等待程序运行完成。
3.  程序运行过程中会输出一些提示信息，最后会显示总共输入多少个png文件。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
