
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "CppSQLite3U.h"
#include <vector>
#include <set>
#include <tuple>

typedef std::set<std::tuple<int, int, int, std::string>> Files;

inline bool IsAllDigital(const char *pT)
{
	for (int i = strlen(pT) - 1; i >= 0; i--)
	{
		if (!isdigit(pT[i]))
		{
			return false;
		}
	}

	return true;
}

#ifdef WIN32

#include <windows.h>
#include <direct.h>
#include <io.h>

#define DIR_SEPARATE '\\'

void Process(const char *lpszPath, Files &files)
{
	char szPath[1024];
	sprintf(szPath, "%s\\*.*", lpszPath);
	struct _finddata_t fileinfo; //文件信息的结构体 
	intptr_t hFind = _findfirst(szPath, &fileinfo); //第一次查找 
	if (hFind == -1)
	{
		return;
	}

	do
	{
		if (fileinfo.name[0] == '.')
		{
			continue;
		}

		if (fileinfo.attrib & _A_SUBDIR)
		{
			sprintf(szPath, "%s\\%s", lpszPath, fileinfo.name);
			Process(szPath, files);
		}
		else if (_stricmp(fileinfo.name + strlen(fileinfo.name) - 4, ".png") == 0)
		{
			sprintf(szPath, "%s\\%s", lpszPath, fileinfo.name);
			std::string name = szPath;
			szPath[strlen(szPath)-4] = 0;
			char *p = strrchr(szPath, DIR_SEPARATE);
			if (p == NULL)
			{
				return;
			}
			p++;
			if (!IsAllDigital(p))
			{
				return;
			}
			int y = atoi(p);
			szPath[p - szPath -1] = 0;
			p = strrchr(szPath, DIR_SEPARATE);
			p++;
			if (!IsAllDigital(p))
			{
				return;
			}
			int x = atoi(p);
			szPath[p - szPath -1] = 0;
			p = strrchr(szPath, DIR_SEPARATE);
			p++;
			if (!IsAllDigital(p))
			{
				return;
			}
			int z = atoi(p);
			files.insert({ z,x,y,name });
		}
	} while (_findnext(hFind, &fileinfo) != -1);
	_findclose(hFind);
}

#else

#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>

#define stricmp strcasecmp
#define DIR_SEPARATE '/'

void Process(const char *lpszPath, Files &files)
{
	DIR *dir = opendir(lpszPath);
	if (dir == NULL)
	{
		perror("opendir");
		return;
	}

	// 2.读取
	// 如果是一个目录，则再次进入该目录读取该目录下的文件，递归
	struct dirent *ptr;
	while ((ptr = readdir(dir)) != NULL)
	{
		char * dname = ptr->d_name;
		if (dname[0] == '.')
		{
			continue;
		}

		char szPath[1024];
		// 判断是否是普通文件还是目录
		if (ptr->d_type == DT_DIR)
		{
			sprintf(szPath, "%s/%s", lpszPath, dname);
			Process(szPath, files);
		}
		else if (stricmp(dname + strlen(dname) - 4, ".png") == 0)
		{
			sprintf(szPath, "%s/%s", lpszPath, dname);
			std::string name = szPath;
			szPath[strlen(szPath) - 4] = 0;
			char *p = strrchr(szPath, DIR_SEPARATE);
			if (p == NULL)
			{
				return;
			}
			p++;
			if (!IsAllDigital(p))
			{
				return;
			}
			int y = atoi(p);
			szPath[p - szPath - 1] = 0;
			p = strrchr(szPath, DIR_SEPARATE);
			p++;
			if (!IsAllDigital(p))
			{
				return;
			}
			int x = atoi(p);
			szPath[p - szPath - 1] = 0;
			p = strrchr(szPath, DIR_SEPARATE);
			p++;
			if (!IsAllDigital(p))
			{
				return;
			}
			int z = atoi(p);
			files.insert(std::make_tuple(z, x, y, name));
		}
	};
	// 关闭目录
	closedir(dir);
}

#endif


int main(int argc, char **argv)
{
    char szT[256];
    if (argc > 1)
    {
        strcpy(szT, argv[1]);
		if (strcmp(szT, ".") == 0 || strcmp(szT, "..") == 0)
		{
			getcwd(szT, sizeof(szT));
			if (strcmp(argv[1], "..") == 0)
			{
				char *p = strrchr(szT, DIR_SEPARATE);
				if (p != NULL)
				{
					szT[p - szT] = 0;
				}
			}
		}
    }
    else
    {
        getcwd(szT, sizeof(szT));
    }

	time_t t1, t2, t3;
	time(&t1);
	Files files;
	Process(szT, files);
	time(&t2);
	printf("文件排序耗时:\t%d\t秒\n", t2 - t1);

	char *p = strrchr(szT, DIR_SEPARATE);
	if (p != NULL)
	{
		sprintf(szT + strlen(szT), "%s", p);
	}
	strcat(szT, ".mbtiles");
	if (access(szT, 0) == 0)
	{
		remove(szT);
	}

	int nCount = 0;
	int nSizeMax = 0;
	CppSQLite3DB db;
	db.open(szT);
	db.setBusyTimeout(100);
	db.execDML("CREATE TABLE metadata (name TEXT, value TEXT)");
	db.execDML("CREATE TABLE tiles (zoom_level INTEGER NOT NULL,tile_column INTEGER NOT NULL,tile_row INTEGER NOT NULL,tile_data BLOB NOT NULL,UNIQUE (zoom_level, tile_column, tile_row) )");
	CppSQLite3Statement stmt = db.compileStatement("insert into tiles VALUES(?,?,?,?)");
	db.execDML("BEGIN TRANSACTION");
	std::vector<unsigned char> vc;
	for ( auto t : files)
	{
		FILE *fp = fopen(std::get<3>(t).c_str(), "rb");
		if (fp != NULL)
		{
			fseek(fp, 0, SEEK_END);
			int nSize = ftell(fp);
			rewind(fp);
			if (nSize > nSizeMax)
			{
				nSizeMax = nSize;
				vc.resize(nSize);
			}
			fread((void*)vc.data(), nSize, 1, fp);
			fclose(fp);

			if (nSize > 0)
			{
				stmt.reset();
				stmt.bind(1, std::get<0>(t));
				stmt.bind(2, std::get<1>(t));
				stmt.bind(3, std::get<2>(t));
				stmt.bind(4, vc.data(), nSize);
				stmt.execDML();
				if ((++nCount % 10000) == 0)
				{
					db.execDML("END TRANSACTION");
					db.execDML("BEGIN TRANSACTION");
					printf("入库:\t%d\t条\n", nCount);
				}
			}
		}
	}

	db.execDML("END TRANSACTION");
	time(&t3);
	printf("入库:\t%d\t条\n数据入库耗时:\t%d\t秒\n按任意键继续。\n", nCount, t3 - t2);
	printf("\n\nTotle:\t%d\t\nTime span:\t%d\tsecond\n", nCount, t3 - t2);

#ifdef WIN32
	getchar();
#endif

    return 0;
}